module Main where

import Control.Exception
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Graphics.Vty (Vty, mkVty)
import qualified Graphics.Vty as Vty
import qualified Data.List as List
import Data.Foldable (toList)
import Data.Maybe (fromMaybe)
import Data.Singletons
import Data.Dependent.Sum
import qualified Data.Dependent.Map as DMap
import qualified System.FSNotify as FS
import Control.Concurrent.Chan.Unagi
import Control.Concurrent (forkIO)
import Control.Monad

import DeutschProcessor.Base
import DeutschProcessor.String
import DeutschProcessor.Grammar
import DeutschProcessor.Feature
import DeutschProcessor.Schema
import DeutschProcessor.Document
import DeutschProcessor.Parser

import Util

data AppState =
    AppStateLoadFailed LineNumber ParseError
  | AppStateDocumentEdit Document DocumentCursor

initAppState :: IO AppState
initAppState = do
  str <- readFile "main.deu"
  case parseDocument str of
    Left (lineNumber, err) -> do
      return (AppStateLoadFailed lineNumber err)
    Right document -> do
      let cursor = initDocumentCursor document
      return (AppStateDocumentEdit document cursor)

main :: IO ()
main = do
  vtyCfg <- fmap (\c -> c { Vty.mouseMode = Just True }) Vty.standardIOConfig
  bracket (mkVty vtyCfg) Vty.shutdown \vty -> do
    (inChan, outChan) <- newChan
    _ <- forkIO $ forever $ do
      ev <- Vty.nextEvent vty
      writeChan inChan (Left ev)
    FS.withManager \fsmgr -> do
      FS.watchDir fsmgr "." (const True) \ev ->
        case ev of
          FS.Modified _ _ _ -> writeChan inChan (Right ())
          _ -> return ()
      appState <- initAppState
      loop vty (readChan outChan) appState

defAttr, backAttr, treeAttr, errAttr, todoAttr, cursAttr :: Vty.Attr
defAttr =
  Vty.defAttr `Vty.withBackColor` Vty.Color240 219
              `Vty.withForeColor` Vty.Color240 238
backAttr = defAttr `Vty.withForeColor` Vty.Color240 225
treeAttr = defAttr `Vty.withForeColor` Vty.Color240 230
errAttr  = defAttr `Vty.withBackColor` Vty.red
todoAttr = defAttr `Vty.withBackColor` Vty.yellow `Vty.withForeColor` Vty.black
cursAttr = defAttr `Vty.withBackColor` Vty.brightYellow `Vty.withForeColor` Vty.Color240 222

loop :: Vty -> IO (Either Vty.Event ()) -> AppState -> IO ()
loop vty nextEvent = loopChanged
  where

    loopChanged :: AppState -> IO ()
    loopChanged appState = do
      region <- Vty.displayBounds (Vty.outputIface vty)
      Vty.update vty (renderAppState region appState)
      loopUnchanged appState

    loopUnchanged :: AppState -> IO ()
    loopUnchanged appState = do
      ev <- nextEvent
      case ev of
        -- Vty.EvKey (Vty.KChar c) _ -> do
        --   let appState' =
        --         appState
        --           { appStateUserInput =
        --               appStateUserInput appState <> Text.singleton c
        --           }
        --   loop vty appState'
        -- Vty.EvKey Vty.KBS _ -> do
        --   let appState' =
        --         appState
        --           { appStateUserInput =
        --               Text.dropEnd 1 (appStateUserInput appState)
        --           }
        --   loop vty appState'
        Left (Vty.EvKey (Vty.KChar 'h') _) -> do
          case moveCursorLeft appState of
            Nothing -> loopUnchanged appState
            Just appState' -> loopChanged appState'
        Left (Vty.EvKey (Vty.KChar 'l') _) -> do
          case moveCursorRight appState of
            Nothing -> loopUnchanged appState
            Just appState' -> loopChanged appState'
        Left (Vty.EvKey (Vty.KChar 'q') _) -> return ()
        Right () -> do
          appState' <- initAppState
          case (appState, appState') of
            (AppStateDocumentEdit doc1 cursor1, AppStateDocumentEdit doc2 _)
              | Just cursor2 <- fmap DocumentSentenceCursor (diffSentenceIndex doc1 doc2)
              -> loopChanged (AppStateDocumentEdit doc2 cursor2)
              | validDocumentCursor doc2 cursor1
              -> loopChanged (AppStateDocumentEdit doc1 cursor1)
            _ -> loopChanged appState'
        Left Vty.EvResize{} -> loopChanged appState
        _ -> loopUnchanged appState

moveCursorLeft :: AppState -> Maybe AppState
moveCursorLeft (AppStateDocumentEdit document documentCursor) = do
  documentCursor' <- moveDocumentCursorLeft document documentCursor
  Just $ AppStateDocumentEdit document documentCursor'
moveCursorLeft _ = Nothing

moveCursorRight :: AppState -> Maybe AppState
moveCursorRight (AppStateDocumentEdit document documentCursor) = do
  documentCursor' <- moveDocumentCursorRight document documentCursor
  Just $ AppStateDocumentEdit document documentCursor'
moveCursorRight _ = Nothing

renderAppState :: Vty.DisplayRegion -> AppState -> Vty.Picture
renderAppState _ (AppStateLoadFailed lineNumber err) =
  Vty.Picture
    { Vty.picCursor = Vty.NoCursor,
      Vty.picLayers = [img],
      Vty.picBackground = Vty.Background ' ' defAttr
    }
  where
    img :: Vty.Image
    img =
      Vty.vertCat [
        Vty.string errAttr "Parse error"
          `Vty.horizJoin` Vty.string defAttr
            (" at line " ++ case lineNumber of
               LineNumber n -> shows n ":"),
        Vty.translateX 2 (renderParseError err)
      ]
renderAppState region (AppStateDocumentEdit document documentCursor) =
  Vty.Picture
    { Vty.picCursor = Vty.NoCursor, -- Vty.Cursor (Text.length (appStateUserInput appState)) 0,
      Vty.picLayers = [renderDocument document documentCursor region],
      Vty.picBackground = Vty.Background ' ' defAttr
    }

renderParseError :: ParseError -> Vty.Image
renderParseError err =
  case err of
    SectionSplitFailed -> Vty.string defAttr "Section split failed"
    BadSection -> Vty.string defAttr "Bad section"
    TokenizationFailure -> Vty.string defAttr "Tokenization failure"
    BadSubsectionCount -> Vty.string defAttr "Bad subsection count"
    StrukturFlexionMismatch -> Vty.string defAttr "Struktur/Flexion mismatch"
    ConflictingFeatureSpec ft (FC' fc1) (FC' fc2) ->
      Vty.vertCat [
        Vty.string defAttr $
          "Conflicting feature specification for " ++
          featureToString (SomeSing ft),
        renderFeature ft fc1
          `Vty.horizJoin` Vty.string defAttr " ≠ "
          `Vty.horizJoin` renderFeature ft fc2
      ]
    RedundantFeatureSpec ft (FC' fc) ->
      Vty.string defAttr "Redundant feature specification for "
      `Vty.horizJoin` renderFeatureWithType ft fc
    FailedToParseWord str ->
      Vty.string defAttr $ "Failed to parse word " ++ show str
    FailedToParseFeatureCon str ->
      Vty.string defAttr $ "Failed to parse feature " ++ show str
    UnusedFeatures features ->
      Vty.string defAttr "Unused features:"
        `Vty.vertJoin` Vty.translateX 2 (Vty.vertCat
          [ renderFeatureWithType ft fc
          | (ft :=> FC' fc) <- DMap.toList features])
    UnbalancedBrackets -> Vty.string defAttr "Unbalanced brackets"

renderDocument :: Document -> DocumentCursor -> Vty.DisplayRegion -> Vty.Image
renderDocument document (DocumentSentenceCursor i) region =
  case Seq.lookup (fromIntegral i) sentences of
    Nothing -> error "impossible: invalid document cursor"
    Just sentence ->
      Vty.string cursAttr ("[" ++ show (i+1) ++ "/" ++ show (Seq.length sentences) ++ "]")
      `Vty.vertJoin` Vty.string defAttr ""
      `Vty.vertJoin` renderSentence sentence region
  where
    sentences = documentSentences document
renderDocument document DocumentUnsegmentedTextCursor region =
  Vty.string cursAttr "[UNSEGMENTED]"
  `Vty.vertJoin` Vty.string defAttr ""
  `Vty.vertJoin` Vty.translateX 2 (renderTokens (documentUnsegmentedText document) region)

renderSentence :: Sentence -> Vty.DisplayRegion -> Vty.Image
renderSentence sentence region =
  Vty.vertCat [
    renderTokens (sentenceTokens sentence) region,
    Vty.string defAttr "",
    Vty.translateX 2 (renderSyntaxTree (sentenceInfo sentence)),
    Vty.string defAttr ""
  ]

renderTokens :: Seq Token -> Vty.DisplayRegion -> Vty.Image
renderTokens = go Vty.emptyImage . List.map renderToken . toList
  where
    go :: Vty.Image -> [Vty.Image] -> Vty.DisplayRegion -> Vty.Image
    go acc [] _ = acc
    go acc tokens@(token:tokens') region@(w, _) =
      let
        acc' = acc `Vty.horizJoin` Vty.string defAttr " " `Vty.horizJoin` token
        nonEmptyAccum = Vty.imageWidth acc > 0  -- See Note [renderTokens: non-empty accum]
        needsLineBreak = Vty.imageWidth acc' > w
      in
        if nonEmptyAccum && needsLineBreak
           then acc `Vty.vertJoin` go Vty.emptyImage tokens region
           else go acc' tokens' region

{- Note [renderTokens: non-empty accum]
---------------------------------------
Fixes an infinite loop when a token doesn't fit the screen. The check should
actually be "at least one token in the accumulator", and the (width acc > 0)
check would break if we started each line with a non-empty image.
-}

renderToken :: Token -> Vty.Image
renderToken (Token wort mpunct) =
  case mpunct of
    Nothing -> renderWort wort
    Just punct -> renderWort wort `Vty.horizJoin` renderPunct punct

renderWort :: Wort -> Vty.Image
renderWort (Wort letters) = renderLetters letters

renderLetters :: Seq Letter -> Vty.Image
renderLetters = Vty.string defAttr . List.map letterToChar . toList

renderPunct :: Punct -> Vty.Image
renderPunct = Vty.char defAttr . punctToChar

renderSyntaxTree :: SyntaxTree (Wort, Value Maybe SFlexion) -> Vty.Image
renderSyntaxTree = fullImage . syntaxTreeImage . renderSyntaxTree'

data LeftImage = LeftImage Int Int ((Int, Int) -> Vty.Image)

toLeftImage :: Vty.Image -> LeftImage
toLeftImage img = LeftImage (Vty.imageWidth img) (Vty.imageHeight img) (const img)

fullImage :: LeftImage -> Vty.Image
fullImage (LeftImage w _ f) = f (0, w)

leftImageHeight, leftImageWidth :: LeftImage -> Int
leftImageHeight (LeftImage _ h _) = h
leftImageWidth  (LeftImage w _ _) = w

leftImageVertJoin :: LeftImage -> LeftImage -> LeftImage
leftImageVertJoin (LeftImage w1 h1 f1) (LeftImage w2 h2 f2) =
  LeftImage (max w1 w2) (h1 + h2) (\ctx -> Vty.vertJoin (f1 ctx) (f2 ctx))

leftImageHorizJoin :: Vty.Image -> LeftImage -> LeftImage
leftImageHorizJoin img1 (LeftImage w2 h2 f2) =
    LeftImage (w1 + w2) (max h1 h2) (\(wLeft, wTotal) -> Vty.horizJoin img1 (f2 (wLeft + w1, wTotal)))
  where
    h1 = Vty.imageHeight img1
    w1 = Vty.imageWidth  img1

data SyntaxTreeImage =
    LeafImage { syntaxTreeImage :: LeftImage }
  | NodeImage { syntaxTreeImage :: LeftImage, connectorY :: Int }

evenCeil :: Int -> Int
evenCeil n = n + (n `mod` 2)

renderSyntaxTree' :: SyntaxTree (Wort, Value Maybe SFlexion) -> SyntaxTreeImage
renderSyntaxTree' (SyntaxLeaf (w, flx)) =
  LeafImage $
    leftImageHorizJoin (renderWort w) $
    LeftImage 0 0 (\ctx -> Vty.horizJoin (dots ctx) (renderValue flx))
  where
    dots (wLeft, wTotal) =
      Vty.string backAttr $
        map (\i -> if even i then ' ' else '·') [wLeft .. evenCeil (wTotal + 2)]
renderSyntaxTree' (SyntaxNode items) =
  if len == 0 then
    LeafImage $
      toLeftImage (Vty.string errAttr "Ø")
  else
    NodeImage
      (foldr1 leftImageVertJoin $
       zipWith (\i img -> padSyntaxTree (getPos i) img) [0..] itemImages)
      (fromMaybe 0 (firstLeafY itemImages))
  where
    itemImages = map renderSyntaxTree' (toList items)
    getPos i =
      case (i == 0, i == len - 1) of
        (True,  False) -> PosFirst
        (False, True)  -> PosLast
        _              -> PosMiddle
    len = Seq.length items

firstLeafY :: [SyntaxTreeImage] -> Maybe Int
firstLeafY = go 0
  where
    go _ [] = Nothing
    go acc (LeafImage _ : _) = Just acc
    go acc (NodeImage img _ : imgs) =
      go (acc + leftImageHeight img) imgs

data Position = PosFirst | PosMiddle | PosLast

padSyntaxTree :: Position -> SyntaxTreeImage -> LeftImage
padSyntaxTree _ (LeafImage img) = img
padSyntaxTree pos (NodeImage img y) = leftImageHorizJoin padding img
  where
    padding = Vty.vertCat $
      replicate y topImg ++
      connectorImg : replicate (leftImageHeight img - y - 1) bottomImg
    (topImg, connectorImg, bottomImg) =
      case pos of
         PosFirst  -> (Vty.string treeAttr "  ", Vty.string treeAttr "┌─", Vty.string treeAttr "│ ")
         PosMiddle -> (Vty.string treeAttr "│ ", Vty.string treeAttr "├─", Vty.string treeAttr "│ ")
         PosLast   -> (Vty.string treeAttr "│ ", Vty.string treeAttr "└─", Vty.string treeAttr "  ")

(<+>) :: Vty.Image -> Vty.Image -> Vty.Image
(<+>) img1 img2 = Vty.horizCat [img1, Vty.string defAttr " ", img2]

renderValue :: Known (SchemaRep schema) => Value Maybe schema -> Vty.Image
renderValue = flattenValue flat known
  where
    flat =
      Flattener {
        fromTypeStr = renderUnspecifiedFeature,
        fromConStr  = renderFeature,
        mergeR      = (<+>)
      }

renderUnspecifiedFeature :: SFeature ft -> Vty.Image
renderUnspecifiedFeature ft =
  Vty.string todoAttr ('?' : featureToString (SomeSing ft))

renderFeatureWithType :: SFeature ft -> SFeatureCon @ft fcon -> Vty.Image
renderFeatureWithType ft fcon =
  Vty.string defAttr (featureToString (SomeSing ft))
    `Vty.horizJoin` Vty.string defAttr " = "
    `Vty.horizJoin` renderFeature ft fcon

renderFeature :: SFeature ft -> SFeatureCon @ft fcon -> Vty.Image
renderFeature ft fcon = Vty.string (attr fcon) (featureConToString (ft :=> FC' fcon))
  where
    attr :: SFeatureCon @ft fcon -> Vty.Attr
    attr SFemininum = defAttr `Vty.withStyle` Vty.italic
    attr SFiniteVerbform = defAttr `Vty.withForeColor` Vty.brightMagenta
    attr SAttributiv = defAttr `Vty.withForeColor` Vty.brightGreen
    attr SNominativ = defAttr `Vty.withForeColor` Vty.blue
    attr SAkkusativ = defAttr `Vty.withForeColor` Vty.brightYellow
    attr SDativ     = defAttr `Vty.withForeColor` Vty.brightRed
    attr _ = defAttr
