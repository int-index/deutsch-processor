module DeutschProcessor.Parser
  ( ParseError(..),
    LineNumber(..),
    parseDocument,
  ) where

import Prelude hiding (lines)
import Data.Void
import Data.Foldable (toList)
import qualified Control.Monad.Combinators as P
import qualified Control.Monad.Combinators.NonEmpty as P.NE
import Data.List.NonEmpty (NonEmpty((:|)))
import qualified Data.List.NonEmpty as NE
import Control.Monad.State
import Control.Monad.Except
import qualified Data.Sequence as Seq
import Data.Sequence (Seq((:<|)))
import Data.Dependent.Sum
import Data.Dependent.Map (DMap)
import qualified Data.Dependent.Map as DMap

import DeutschProcessor.Base
import DeutschProcessor.Document
import DeutschProcessor.String
import DeutschProcessor.Feature
import DeutschProcessor.Schema
import DeutschProcessor.Grammar
import Util

data ParseError where
  SectionSplitFailed :: ParseError
  BadSection :: ParseError
  TokenizationFailure :: ParseError
  BadSubsectionCount :: ParseError
  StrukturFlexionMismatch :: ParseError
  ConflictingFeatureSpec :: SFeature a -> FeatureCon' a -> FeatureCon' a -> ParseError
  RedundantFeatureSpec :: SFeature a -> FeatureCon' a -> ParseError
  FailedToParseWord :: String -> ParseError
  FailedToParseFeatureCon :: String -> ParseError
  UnusedFeatures :: DMap SFeature FeatureCon' -> ParseError
  UnbalancedBrackets :: ParseError

deriving instance Show ParseError

newtype LineNumber = LineNumber Word
  deriving (Show)

newtype Indent = Indent Word
  deriving (Eq, Ord, Show)

data Line' e = EmptyLine !e | Line LineNumber Indent String
  deriving (Show)

type Line = Line' ()

type NonEmptyLine = Line' Void

splitIndent :: String -> (Indent, String)
splitIndent = go 0
  where
    go :: Word -> String -> (Indent, String)
    go !n (' ':s) = go (n+1) s
    go !n s = (Indent n, s)

splitLineStr :: String -> (String, String)
splitLineStr = go id
  where
    go acc ""         = (acc "", "")
    go acc ('\n':str) = (acc "", str)
    go acc (c:str)    = go (acc . (c:)) str

toLines :: String -> [Line]
toLines = go 1
  where
    go :: Word -> String -> [Line]
    go !n str =
      case splitIndent str of
        (_, "")   -> []
        (_, '\n':s) -> EmptyLine () : go (n+1) s
        (indent, s) ->
          let (lineStr, remainder) = splitLineStr s
          in Line (LineNumber n) indent lineStr : go (n+1) remainder

data Section =
  Section {
    sectionHeader :: NonEmpty NonEmptyLine,
    sectionSubsections :: [Section]
  }
  deriving (Show)

sectionLineNumber :: Section -> LineNumber
sectionLineNumber section =
  case sectionHeader section of
    Line lineNumber _ _ :| _ -> lineNumber

type LinesM = StateT [Line] Maybe

skipEmptyLines :: LinesM ()
skipEmptyLines =
  P.skipMany $ StateT \lines ->
    case lines of
      EmptyLine{} : lines' -> Just ((), lines')
      _ -> Nothing

getLinesAtIndent :: Indent -> LinesM (NonEmpty NonEmptyLine)
getLinesAtIndent currentIndent = do
  P.NE.some $ StateT \lines ->
    case lines of
      line : lines' | Line lineNumber indent lineStr <- line, indent == currentIndent ->
        Just (Line lineNumber indent lineStr, lines')
      _ -> Nothing

peekNextIndent :: [Line] -> Maybe Indent
peekNextIndent [] = Nothing
peekNextIndent (EmptyLine{} : lines) = peekNextIndent lines
peekNextIndent (Line _ indent _ : _) = Just indent

splitSection :: Indent -> LinesM Section
splitSection currentIndent = do
  skipEmptyLines
  sectionHeader <- getLinesAtIndent currentIndent
  sectionSubsections <- do
    mNextIndent <- gets peekNextIndent
    case mNextIndent of
      Just nextIndent | nextIndent > currentIndent ->
        P.many (splitSection nextIndent)
      _ -> return []
  return Section{sectionHeader, sectionSubsections}

type ParseErrM = Except (LineNumber, ParseError)

splitSections :: [Line] -> ParseErrM [Section]
splitSections lines = do
  let splitSectionsM = P.many (splitSection (Indent 0))
  case runStateT splitSectionsM lines of
    Nothing -> throwError (LineNumber 0, SectionSplitFailed)
    Just (sections, remainingLines) -> do
      ensureEmpty remainingLines
      return sections
  where
    ensureEmpty :: [Line] -> ParseErrM ()
    ensureEmpty [] = return ()
    ensureEmpty (EmptyLine{} : lines') = ensureEmpty lines'
    ensureEmpty (Line lineNumber _ _ : _) = throwError (lineNumber, SectionSplitFailed)

expectNoSubsections :: Section -> ParseErrM ()
expectNoSubsections section =
  unless (null (sectionSubsections section)) $
    throwError (sectionLineNumber section, BadSubsectionCount)

expectTwoSubsections :: Section -> ParseErrM (Section, Section)
expectTwoSubsections section =
  case sectionSubsections section of
    [s1, s2] -> return (s1, s2)
    _ -> throwError (sectionLineNumber section, BadSubsectionCount)

matchSentenceSection :: Section -> ParseErrM Sentence
matchSentenceSection section =
  do
    sentenceTokens <- matchTokenLines (\_ -> parseTokens) (sectionHeader section)
    (structureSubsection, flexionSubsection) <- expectTwoSubsections section
    struktur <- matchStrukturSection structureSubsection
    flexion <- matchFlexionSection flexionSubsection
    case fillSyntaxTree flexion struktur of
      Nothing -> throwError (sectionLineNumber section, StrukturFlexionMismatch)
      Just sentenceInfo -> return Sentence{sentenceTokens, sentenceInfo}

parseTreeTokens' :: LineNumber -> String -> Maybe (Seq (LineNumber, TreeToken))
parseTreeTokens' lineNumber str =
  (fmap . fmap)
    (\treeToken -> (lineNumber, treeToken))
    (parseTreeTokens str)

matchStrukturSection :: Section -> ParseErrM (SyntaxTree Wort)
matchStrukturSection section = do
  expectNoSubsections section
  treeTokens <- matchTokenLines parseTreeTokens' (sectionHeader section)
  (tree, otherTokens) <- runStateT matchTree (toList treeTokens)
  case otherTokens of
    [] -> return tree
    (lineNumber, TreeRightBr) : _ -> throwError (lineNumber, UnbalancedBrackets)
    _ -> error "impossible: matchTree must consume all tokens"
  return tree

matchTree :: StateT [(LineNumber, TreeToken)] ParseErrM (SyntaxTree Wort)
matchTree = do
  trees <- matchTrees
  case trees of
    tree :<| Seq.Empty -> return tree
    _ -> return (SyntaxNode trees)

matchTree1 :: [(LineNumber, TreeToken)] -> ParseErrM (Maybe (SyntaxTree Wort), [(LineNumber, TreeToken)])
matchTree1 tokens@[] = return (Nothing, tokens)
matchTree1 tokens@((_, TreeRightBr) : _) = return (Nothing, tokens)
matchTree1 ((_, TreeHead w) : tokens) = return (Just (SyntaxLeaf w), tokens)
matchTree1 ((lineNumber, TreeLeftBr) : tokens) = do
  (subtrees, tokens') <- runStateT matchTrees tokens
  case tokens' of
    (_, TreeRightBr) : tokens'' -> return (Just (SyntaxNode subtrees), tokens'')
    _ -> throwError (lineNumber, UnbalancedBrackets)

matchTrees :: StateT [(LineNumber, TreeToken)] ParseErrM (Seq (SyntaxTree Wort))
matchTrees = go mempty
  where
    go :: SeqBuilder (SyntaxTree Wort) -> StateT [(LineNumber, TreeToken)] ParseErrM (Seq (SyntaxTree Wort))
    go acc = do
      mSubtree <- StateT matchTree1
      case mSubtree of
        Nothing -> return (runSeqBuilder acc)
        Just subtree -> go (acc <> singletonSeqBuilder subtree)

matchFlexionSection :: Section -> ParseErrM [(Wort, Value Maybe SFlexion)]
matchFlexionSection section = do
  expectNoSubsections section
  mapM matchFlexionLine (NE.toList (sectionHeader section))

matchFlexionLine :: NonEmptyLine -> ParseErrM (Wort, Value Maybe SFlexion)
matchFlexionLine (Line lineNumber _ lineStr) =
  do
    let (headWord, featureWords) =
          case words lineStr of
            [] -> error "impossible: non-empty line contains no words"
            (w1:ws) -> (w1, ws)
    wort <-
      case parseWort headWord of
        Nothing -> throwError (lineNumber, FailedToParseWord headWord)
        Just wort -> return wort
    fcons <-
      forM featureWords \featureWord ->
        case stringToFeatureCon featureWord of
          Nothing -> throwError (lineNumber, FailedToParseFeatureCon featureWord)
          Just fcon -> return fcon
    features <- groupFeatures DMap.empty fcons
    let (flexion, extraFeatures) = toFlexion features
    unless (DMap.null extraFeatures) $
      throwError (lineNumber, UnusedFeatures extraFeatures)
    return (wort, flexion)
  where
    groupFeatures :: DMap SFeature FeatureCon' -> [DSum SFeature FeatureCon'] -> ParseErrM (DMap SFeature FeatureCon')
    groupFeatures acc [] = return acc
    groupFeatures acc ((mt :=> mm) : mms) =
      case DMap.lookup mt acc of
        Just mm' -> do
          let err | mm' == mm = RedundantFeatureSpec mt mm
                  | otherwise = ConflictingFeatureSpec mt mm mm'
          throwError (lineNumber, err)
        Nothing -> groupFeatures (DMap.insert mt mm acc) mms

matchUnsegmentedSection :: Section -> ParseErrM (Seq Token)
matchUnsegmentedSection section = do
  expectNoSubsections section
  matchTokenLines (\_ -> parseTokens) (sectionHeader section)

matchTokenLines ::
  forall tok.
  (LineNumber -> String -> Maybe (Seq tok)) ->
  NonEmpty NonEmptyLine ->
  ParseErrM (Seq tok)
matchTokenLines matchStr lines = go Seq.empty (NE.toList lines)
  where
    go :: Seq tok -> [NonEmptyLine] -> ParseErrM (Seq tok)
    go acc [] = return acc
    go acc (Line lineNumber _ str : lines') =
      case matchStr lineNumber str of
        Nothing -> throwError (lineNumber, TokenizationFailure)
        Just tokens -> go (acc <> tokens) lines'

matchDocumentSections :: StateT [Section] ParseErrM Document
matchDocumentSections = StateT (go mempty)
  where
    finalize_doc :: SeqBuilder Sentence -> Seq Token -> Document
    finalize_doc acc unsegmented = Document (runSeqBuilder acc) unsegmented

    go :: SeqBuilder Sentence -> [Section] -> ParseErrM (Document, [Section])
    go acc [] = return (finalize_doc acc Seq.empty, [])
    go acc (section : sections') = do
      case runExcept (matchSentenceSection section) of
        Right sentence -> go (acc <> singletonSeqBuilder sentence) sections'
        Left sentenceErr ->
          case runExcept (matchUnsegmentedSection section) of
            Right unsegmented -> return (finalize_doc acc unsegmented, sections')
            Left _ -> throwError sentenceErr

parseDocument :: String -> Either (LineNumber, ParseError) Document
parseDocument str = runExcept do
  sections <- splitSections (toLines str)
  (document, otherSections) <- runStateT matchDocumentSections sections
  case otherSections of
    [] -> return document
    section : _ -> throwError (sectionLineNumber section, BadSection)
