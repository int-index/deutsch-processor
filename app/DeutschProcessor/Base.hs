module DeutschProcessor.Base where

import Data.Sequence (Seq)

data LetterCase = UpperCase | LowerCase
  deriving (Eq, Ord, Show)

data Letter
  = LetterA LetterCase
  | LetterÄ LetterCase
  | LetterB LetterCase
  | LetterC LetterCase
  | LetterD LetterCase
  | LetterE LetterCase
  | LetterF LetterCase
  | LetterG LetterCase
  | LetterH LetterCase
  | LetterI LetterCase
  | LetterJ LetterCase
  | LetterK LetterCase
  | LetterL LetterCase
  | LetterM LetterCase
  | LetterN LetterCase
  | LetterO LetterCase
  | LetterÖ LetterCase
  | LetterP LetterCase
  | LetterQ LetterCase
  | LetterR LetterCase
  | LetterS LetterCase
  | LetterT LetterCase
  | LetterU LetterCase
  | LetterÜ LetterCase
  | LetterV LetterCase
  | LetterW LetterCase
  | LetterX LetterCase
  | LetterY LetterCase
  | LetterZ LetterCase
  | Letterß
  deriving (Eq, Ord, Show)

data Punct = Comma | Period
  deriving (Eq, Ord, Show)

newtype Wort = Wort (Seq Letter)
  deriving (Eq, Ord, Show)

data Token = Token Wort (Maybe Punct)
  deriving (Eq, Ord, Show)
