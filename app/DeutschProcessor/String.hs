module DeutschProcessor.String
  ( letterToChar,
    punctToChar,
    featureConToString,
    featureToString,
    charToLetter,
    charToPunct,
    stringToFeatureCon,
    stringToFeature,
    parseTokens,
    parseTreeTokens,
    parseWort,
    TreeToken(..),
  )
where

import Data.Sequence (Seq)
import Prelude hiding (Word)
import Text.Regex.Applicative (RE)
import qualified Data.Char as Char
import qualified Data.Sequence as Seq
import qualified Text.Regex.Applicative as RE
import Data.Singletons
import Data.Dependent.Sum

import DeutschProcessor.Base
import DeutschProcessor.Feature

import Util

letterToChar :: Letter -> Char
charToLetter :: Char -> Maybe Letter
(letterToChar, charToLetter) =
  mapping
    [ (LetterA UpperCase, 'A'),
      (LetterA LowerCase, 'a'),
      (LetterÄ UpperCase, 'Ä'),
      (LetterÄ LowerCase, 'ä'),
      (LetterB UpperCase, 'B'),
      (LetterB LowerCase, 'b'),
      (LetterC UpperCase, 'C'),
      (LetterC LowerCase, 'c'),
      (LetterD UpperCase, 'D'),
      (LetterD LowerCase, 'd'),
      (LetterE UpperCase, 'E'),
      (LetterE LowerCase, 'e'),
      (LetterF UpperCase, 'F'),
      (LetterF LowerCase, 'f'),
      (LetterG UpperCase, 'G'),
      (LetterG LowerCase, 'g'),
      (LetterH UpperCase, 'H'),
      (LetterH LowerCase, 'h'),
      (LetterI UpperCase, 'I'),
      (LetterI LowerCase, 'i'),
      (LetterJ UpperCase, 'J'),
      (LetterJ LowerCase, 'j'),
      (LetterK UpperCase, 'K'),
      (LetterK LowerCase, 'k'),
      (LetterL UpperCase, 'L'),
      (LetterL LowerCase, 'l'),
      (LetterM UpperCase, 'M'),
      (LetterM LowerCase, 'm'),
      (LetterN UpperCase, 'N'),
      (LetterN LowerCase, 'n'),
      (LetterO UpperCase, 'O'),
      (LetterO LowerCase, 'o'),
      (LetterÖ UpperCase, 'Ö'),
      (LetterÖ LowerCase, 'ö'),
      (LetterP UpperCase, 'P'),
      (LetterP LowerCase, 'p'),
      (LetterQ UpperCase, 'Q'),
      (LetterQ LowerCase, 'q'),
      (LetterR UpperCase, 'R'),
      (LetterR LowerCase, 'r'),
      (LetterS UpperCase, 'S'),
      (LetterS LowerCase, 's'),
      (LetterT UpperCase, 'T'),
      (LetterT LowerCase, 't'),
      (LetterU UpperCase, 'U'),
      (LetterU LowerCase, 'u'),
      (LetterÜ UpperCase, 'Ü'),
      (LetterÜ LowerCase, 'ü'),
      (LetterV UpperCase, 'V'),
      (LetterV LowerCase, 'v'),
      (LetterW UpperCase, 'W'),
      (LetterW LowerCase, 'w'),
      (LetterX UpperCase, 'X'),
      (LetterX LowerCase, 'x'),
      (LetterY UpperCase, 'Y'),
      (LetterY LowerCase, 'y'),
      (LetterZ UpperCase, 'Z'),
      (LetterZ LowerCase, 'z'),
      (Letterß, 'ß')
    ]

punctToChar :: Punct -> Char
charToPunct :: Char -> Maybe Punct
(punctToChar, charToPunct) =
  mapping
    [ (Comma, ','),
      (Period, '.')
    ]

featureConToString :: DSum SFeature FeatureCon' -> String
stringToFeatureCon :: String -> Maybe (DSum SFeature FeatureCon')
(featureConToString, stringToFeatureCon) =
  mapping
    [ (SNumerus :=> FC' SSingular, "n=1"),
      (SNumerus :=> FC' SPlural, "n>1"),
      (SGenus :=> FC' SNeutrum, "n"),
      (SGenus :=> FC' SMaskulinum, "m"),
      (SGenus :=> FC' SFemininum, "f"),
      (SKasus :=> FC' SNominativ, "Nom"),
      (SKasus :=> FC' SAkkusativ, "Akk"),
      (SKasus :=> FC' SDativ, "Dat"),
      (SKasus :=> FC' SGenitiv, "Gen"),
      (SPerson :=> FC' SErstePerson, "1.P"),
      (SPerson :=> FC' SZweitePerson, "2.P"),
      (SPerson :=> FC' SDrittePerson, "3.P"),
      (SModus :=> FC' SIndikativ, "Ind"),
      (SModus :=> FC' SKonjunktiv, "Konj"),
      (SModus :=> FC' SImperativ, "Imp"),
      (STempus :=> FC' SPräteritum, "Vrg"),  -- Vergangenheit
      (STempus :=> FC' SPräsens, "Ggw"),     -- Gegenwart
      (SVerbform :=> FC' SInfinitiv, "Inf"),
      (SVerbform :=> FC' SPartizipPräsens, "PzpPräs"),
      (SVerbform :=> FC' SPartizipPerfekt, "PzpPerf"),
      (SVerbform :=> FC' SFiniteVerbform, "fin"),
      (SDeklination :=> FC' SSchwach, "w.D."), -- sch(w)ache Deklination, (w)eak declension
      (SDeklination :=> FC' SStark, "s.D."),   -- (s)tarke Deklination,  (s)trong declension
      (SKomparation :=> FC' SPositiv, "Pos"),
      (SKomparation :=> FC' SKomparativ, "Komp"),
      (SKomparation :=> FC' SSuperlativ, "Sup"),
      (SAdjektivform :=> FC' SPrädikativ, "Präd"),
      (SAdjektivform :=> FC' SAttributiv, "Attr"),
      (SWortart :=> FC' SVerb, "V"),
      (SWortart :=> FC' SSubstantiv, "S"),
      (SWortart :=> FC' SAdjektiv, "Adj"),
      (SWortart :=> FC' SPronomen, "Pron"),
      (SWortart :=> FC' SArtikelwort, "Art"),
      (SWortart :=> FC' SAdverb, "Adv"),
      (SWortart :=> FC' SPartikel, "Part"),
      (SWortart :=> FC' SPräposition, "Pp"),
      (SWortart :=> FC' SJunktion, "J"),
      (SWortart :=> FC' SVerbpartikel, "t.P.")  -- trennbares Präfix
    ]

featureToString :: SomeSing Feature -> String
stringToFeature :: String -> Maybe (SomeSing Feature)
(featureToString, stringToFeature) =
  mapping
    [ (SomeSing SNumerus, "Numerus"),
      (SomeSing SGenus, "Genus"),
      (SomeSing SPerson, "Person"),
      (SomeSing SKasus, "Kasus"),
      (SomeSing SVerbform, "Verbform"),
      (SomeSing SModus, "Modus"),
      (SomeSing STempus, "Tempus"),
      (SomeSing SAdjektivform, "Adjektivform"),
      (SomeSing SDeklination, "Deklination"),
      (SomeSing SKomparation, "Komparation"),
      (SomeSing SWortart, "Wortart")
    ]

reLetter :: RE Char Letter
reLetter = RE.msym charToLetter

rePunct :: RE Char Punct
rePunct = RE.msym charToPunct

reWort :: RE Char Wort
reWort = fmap (Wort . Seq.fromList) (RE.some reLetter)

reToken :: RE Char Token
reToken = do
  wort <- reWort
  mpunct <- RE.optional rePunct
  pure (Token wort mpunct)

reSkipSpaces :: RE Char ()
reSkipSpaces = RE.reFoldl RE.Greedy const () (RE.psym Char.isSpace)

reTokens :: RE Char [Token]
reTokens = reSkipSpaces *> RE.some (reToken <* reSkipSpaces)

data TreeToken = TreeLeftBr | TreeHead Wort | TreeRightBr
  deriving (Eq, Ord, Show)

reTreeToken :: RE Char TreeToken
reTreeToken =
  RE.asum [
    TreeLeftBr <$ RE.sym '[',
    TreeRightBr <$ RE.sym ']',
    fmap TreeHead reWort
  ]

reTreeTokens :: RE Char [TreeToken]
reTreeTokens = reSkipSpaces *> RE.many (reTreeToken <* reSkipSpaces)

parseTokens :: String -> Maybe (Seq Token)
parseTokens = fmap Seq.fromList . RE.match reTokens

parseTreeTokens :: String -> Maybe (Seq TreeToken)
parseTreeTokens = fmap Seq.fromList . RE.match reTreeTokens

parseWort :: String -> Maybe Wort
parseWort = RE.match reWort
