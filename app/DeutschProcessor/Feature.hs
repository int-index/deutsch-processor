{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS -Wno-unused-top-binds #-}

module DeutschProcessor.Feature
  ( Feature(..),
    SFeature(..),
    FeatureCon(..),
    SFeatureCon(..),
    eqSFeatureCon,
    leqSFeatureCon,
    FeatureCon'(..),
  ) where

import Data.Kind
import Data.Singletons.TH
import Data.Singletons.TH.Options
import Data.Constraint.Extras.TH
import Data.Ord.Singletons
import Prelude.Singletons
import Data.GADT.Compare
import Data.GADT.Show
import Data.Type.Equality (TestEquality(testEquality))
import GHC.Exts (dataToTag#, (==#), (<=#), isTrue#)
import Data.Singletons.Base.SomeSing ()
import Unsafe.Coerce (unsafeCoerce)

type Feature :: Type
data Feature =
    Numerus
  | Genus
  | Kasus
  | Person
  | Modus
  | Tempus
  | Verbform
  | Deklination
  | Komparation
  | Adjektivform
  | Wortart
  deriving (Eq, Ord, Show)

type FeatureCon :: Feature -> Type
data FeatureCon ft where
  Singular, Plural :: FeatureCon Numerus
  Neutrum, Maskulinum, Femininum :: FeatureCon Genus
  Nominativ, Akkusativ, Dativ, Genitiv :: FeatureCon Kasus
  ErstePerson, ZweitePerson, DrittePerson :: FeatureCon Person
  Indikativ, Konjunktiv, Imperativ :: FeatureCon Modus
  Präteritum, Präsens :: FeatureCon Tempus
  Infinitiv, PartizipPräsens, PartizipPerfekt, FiniteVerbform :: FeatureCon Verbform
  Schwach, Stark :: FeatureCon Deklination
  Positiv, Komparativ, Superlativ :: FeatureCon Komparation
  Prädikativ, Attributiv :: FeatureCon Adjektivform
  Verb, Substantiv, Adjektiv, Pronomen, Artikelwort, Adverb, Partikel,
    Präposition, Junktion, Verbpartikel :: FeatureCon Wortart

deriving instance Eq (FeatureCon ft)
deriving instance Ord (FeatureCon ft)
deriving instance Show (FeatureCon ft)

withOptions defaultOptions{genSingKindInsts = False} do
  genSingletons [ ''Feature, ''FeatureCon ]

singDecideInstance ''Feature
singEqInstance ''Feature
singOrdInstance ''Feature
deriveArgDict ''SFeature

instance GEq SFeature where
  geq = testEquality

instance GCompare SFeature where
  gcompare ft1 ft2 =
    case geq ft1 ft2 of
      Just Refl -> GEQ
      Nothing ->
        case ft1 %< ft2 of
          STrue  -> GLT
          SFalse -> GGT

instance GShow SFeature where
  gshowsPrec = showsPrec

deriving instance Show (SFeature ft)
deriving instance Show (SFeatureCon fcon)

eqSFeatureCon :: SFeatureCon fcon1 -> SFeatureCon fcon2 -> Bool
eqSFeatureCon fc1 fc2 = isTrue# (dataToTag# fc1 ==# dataToTag# fc2)

leqSFeatureCon :: SFeatureCon fcon1 -> SFeatureCon fcon2 -> Bool
leqSFeatureCon fc1 fc2 =  isTrue# (dataToTag# fc1 <=# dataToTag# fc2)

instance TestEquality SFeatureCon where
  testEquality fc1 fc2 =
    if eqSFeatureCon fc1 fc2
    then Just (unsafeCoerce Refl)
    else Nothing

data FeatureCon' fc where
  FC' :: SFeatureCon @fc fcon -> FeatureCon' fc

instance Show (FeatureCon' fc) where
  showsPrec n (FC' x) = showsPrec n x

instance Eq (FeatureCon' fc) where
  FC' x1 == FC' x2 = eqSFeatureCon x1 x2

instance Ord (FeatureCon' fc) where
  FC' x1 <= FC' x2 = leqSFeatureCon x1 x2
