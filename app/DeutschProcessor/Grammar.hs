module DeutschProcessor.Grammar where

import Data.Sequence (Seq)
import Data.Dependent.Map (DMap)
import Control.Monad.State

import DeutschProcessor.Base
import DeutschProcessor.Feature
import DeutschProcessor.Schema
import Util

type SNumerus :: Schema
type SNumerus = S '[ Singular ::: '[], Plural ::: '[] ]

type SGenus :: Schema
type SGenus = S '[ Neutrum ::: '[], Maskulinum ::: '[], Femininum ::: '[] ]

type SKasus :: Schema
type SKasus = S '[ Nominativ ::: '[], Akkusativ ::: '[], Dativ ::: '[], Genitiv ::: '[] ]

type SPerson :: Schema
type SPerson = S '[ ErstePerson ::: '[], ZweitePerson ::: '[], DrittePerson ::: '[] ]

type STempus :: Schema
type STempus = S '[ Präteritum ::: '[], Präsens ::: '[] ]

type SNumerusGenus :: Schema
type SNumerusGenus =
  S '[
    Singular ::: '[ SGenus ],
    Plural   ::: '[]
  ]

type SPersonNumerusGenus :: Schema
type SPersonNumerusGenus =
  S '[
    ErstePerson  ::: '[ SNumerus ],
    ZweitePerson ::: '[ SNumerus ],
    DrittePerson ::: '[ SNumerusGenus ]
  ]

type SModusPersonTempus :: Schema
type SModusPersonTempus =
  S '[
    Indikativ  ::: '[ SPerson, STempus ],
    Konjunktiv ::: '[ SPerson, STempus ],
    Imperativ  ::: '[]
  ]

type SKomparation :: Schema
type SKomparation = S '[ Positiv ::: '[], Komparativ ::: '[], Superlativ ::: '[] ]

type SDeklination :: Schema
type SDeklination = S '[ Schwach ::: '[], Stark ::: '[] ]

type SAdjektivform :: Schema
type SAdjektivform =
  S '[
    Prädikativ ::: '[ SKomparation ],
    Attributiv ::: '[ SKomparation, SNumerusGenus, SKasus, SDeklination ]
  ]

type SVerbform :: Schema
type SVerbform =
  S '[
    Infinitiv       ::: '[],
    PartizipPräsens ::: '[ SAdjektivform ],
    PartizipPerfekt ::: '[ SAdjektivform ],
    FiniteVerbform  ::: '[ SNumerus, SModusPersonTempus ]
  ]

type SFlexion :: Schema
type SFlexion =
  S '[
    Verb         ::: '[ SVerbform ],
    Substantiv   ::: '[ SNumerusGenus, SKasus ],
    Adjektiv     ::: '[ SAdjektivform ],
    Pronomen     ::: '[ SPersonNumerusGenus, SKasus ],
    Artikelwort  ::: '[ SNumerusGenus, SKasus ],
    Adverb       ::: '[],
    Partikel     ::: '[],
    Präposition  ::: '[],
    Junktion     ::: '[],
    Verbpartikel ::: '[]
  ]

sFlexionRep :: SchemaRep SFlexion
sFlexionRep = known

-- snd: residual features, must be empty
toFlexion :: DMap SFeature FeatureCon' -> (Value Maybe SFlexion, DMap SFeature FeatureCon')
toFlexion = runState (takeValue sFlexionRep)

data SyntaxTree a = SyntaxLeaf a | SyntaxNode (Seq (SyntaxTree a))
  deriving (Eq, Ord, Show)
  deriving (Functor, Foldable, Traversable)

fillSyntaxTree :: forall a. [(Wort, a)] -> SyntaxTree Wort -> Maybe (SyntaxTree (Wort, a))
fillSyntaxTree ws b =
  do
    (b', ws') <- runStateT (mapM applyMatching b) ws
    guard (null ws')
    return b'
  where
    applyMatching :: Wort -> StateT [(Wort, a)] Maybe (Wort, a)
    applyMatching w = do
      ((w', a):pairs) <- get
      put pairs
      guard (w == w')
      return (w, a)
