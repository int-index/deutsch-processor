{-# LANGUAGE TemplateHaskell #-}

module DeutschProcessor.Schema where

import Data.Kind
import Data.Singletons
import Control.Monad.State
import Data.Type.Equality
import Data.Functor.Classes
import Data.Dependent.Map (DMap)
import qualified Data.Dependent.Map as DMap

import DeutschProcessor.Feature
import Util

type ConSpec :: Feature -> Type
type ConSpec ft = KV (FeatureCon ft) [Schema]

type Schema :: Type
data Schema where
  S :: forall ft. [ConSpec ft] -> Schema

type ConSpecRep :: forall (ft :: Feature) -> ConSpec ft -> Type
data ConSpecRep ft conspec where
  ConSpecRep ::
    SingI fcon =>
    ListOf SchemaRep schemas ->
    ConSpecRep ft (fcon ::: schemas)

type SchemaRep :: Schema -> Type
data SchemaRep schema where
  SchemaRep  ::
    SingI ft =>
    ListOf (ConSpecRep ft) conspecs ->
    SchemaRep (S @ft conspecs)

instance
    ( SingI fcon,
      Known (ListOf SchemaRep schemas)
    ) =>
    Known (ConSpecRep ft (fcon ::: schemas))
  where
    known = ConSpecRep known

instance
    ( SingI ft,
      Known (ListOf (ConSpecRep ft) conspecs)
    ) =>
    Known (SchemaRep (S @ft conspecs))
  where
    known = SchemaRep known

type Value :: (Type -> Type) -> Schema -> Type
data Value f schema where
  V :: f (Index fcon schemas conspecs, Fields f schemas) -> Value f (S @ft conspecs)

deriving instance (forall x. Show x => Show (f x)) => Show (Value f schema)

instance Eq1 f => Eq (Value f schema) where
  V lhs == V rhs = liftEq eqPairs lhs rhs
    where
      eqPairs ::
        (Index fcon schemas conspecs, Fields f schemas) ->
        (Index fcon' schemas' conspecs, Fields f schemas') ->
        Bool
      eqPairs (i, flds) (j, flds') =
        case eqIndices i j of
          Just Refl ->  flds == flds'
          Nothing -> False

      eqIndices ::
        (Index fcon schemas conspecs) ->
        (Index fcon' schemas' conspecs) ->
        Maybe (schemas :~: schemas')
      eqIndices IZ IZ = Just Refl
      eqIndices (IS i) (IS j) = eqIndices i j
      eqIndices _ _ = Nothing

type Fields :: (Type -> Type) -> [Schema] -> Type
type Fields f = ListOf (Value f)

takeValue :: SchemaRep schema -> State (DMap SFeature FeatureCon') (Value Maybe schema)
takeValue (SchemaRep @ft conspecs) = do
  mfc <- takeFeatureCon (sing @ft)
  case mfc of
    Nothing -> return (V Nothing)
    Just (FC' fc) -> do
      matchIndex fc conspecs
        (return (V Nothing))
        \i schemas -> do
          flds <- takeFields schemas
          return (V (Just (i, flds)))

takeFields ::
  ListOf SchemaRep schemas ->
  State (DMap SFeature FeatureCon') (Fields Maybe schemas)
takeFields Nil = return Nil
takeFields (schema :& schemas) = do
  fld <- takeValue schema
  flds <- takeFields schemas
  return (fld :& flds)

takeFeatureCon :: SFeature ft -> State (DMap SFeature FeatureCon') (Maybe (FeatureCon' ft))
takeFeatureCon mt =
  state \mms ->
    case DMap.lookup mt mms of
      Nothing -> (Nothing, mms)
      Just a -> (Just a, DMap.delete mt mms)

matchIndex ::
  SFeatureCon @ft fcon ->
  ListOf (ConSpecRep ft) conspecs ->
  r ->
  (forall schemas. Index fcon schemas conspecs -> ListOf SchemaRep schemas -> r) ->
  r
matchIndex _ Nil contFail _ = contFail
matchIndex fc (ConSpecRep @_ @fc' schemas :& conspecs) contFail contSuccess =
  case testEquality fc (sing @fc') of
    Nothing -> matchIndex fc conspecs contFail (contSuccess . IS)
    Just Refl -> contSuccess IZ schemas

data Flattener r =
  Flattener {
    fromTypeStr :: forall ft. SFeature ft -> r,
    fromConStr  :: forall ft fcon. SFeature ft -> SFeatureCon @ft fcon -> r,
    mergeR      :: r -> r -> r
  }

flattenValue ::
  forall schema r. Flattener r ->
    SchemaRep schema -> Value Maybe schema -> r
flattenValue Flattener{fromTypeStr} (SchemaRep @ft _) (V Nothing) =
  fromTypeStr (sing @ft)
flattenValue flat@Flattener{fromConStr} (SchemaRep @ft all_conspecs) (V (Just (i, v))) =
    go all_conspecs i v
  where
    go :: ListOf (ConSpecRep ft) conspecs -> Index fcon schemas conspecs -> Fields Maybe schemas -> r
    go Nil j _ = indexOfEmpty j
    go (ConSpecRep @_ @fcon schemas :& _) IZ flds =
        flattenFields flat schemas flds $
          fromConStr (sing @ft) (sing @fcon)
    go (_ :& conspecs) (IS j) flds = go conspecs j flds

flattenFields ::
  forall schema r.  Flattener r ->
    ListOf SchemaRep schema -> Fields Maybe schema -> r -> r
flattenFields _ Nil Nil = id
flattenFields flat@Flattener{mergeR} (schema :& schemas) (fld :& flds) =
  \acc ->
    flattenFields flat schemas flds $
      acc `mergeR` flattenValue flat schema fld
