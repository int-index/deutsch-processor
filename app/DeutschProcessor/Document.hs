module DeutschProcessor.Document where

import Data.Sequence (Seq((:|>)))
import qualified Data.Sequence as Seq

import DeutschProcessor.Base
import DeutschProcessor.Schema
import DeutschProcessor.Grammar

data Document =
  Document {
    documentSentences :: Seq Sentence,
    documentUnsegmentedText :: Seq Token   -- remaining text that is yet to undergo segmentation
  }
  deriving Show

maxSentenceIndex :: Document -> Maybe Word
maxSentenceIndex Document{documentSentences}
  | n > 0 = Just $ fromIntegral (Seq.length documentSentences - 1)
  | otherwise = Nothing
  where n = Seq.length documentSentences

data Sentence =
  Sentence {
    sentenceTokens :: Seq Token,  -- reference (before processing)
    sentenceInfo :: SyntaxTree (Wort, Value Maybe SFlexion)  -- current analysis (possibly incomplete)
  }
  deriving (Eq, Show)

data DocumentCursor =
    DocumentSentenceCursor Word
  | DocumentUnsegmentedTextCursor

initDocumentCursor :: Document -> DocumentCursor
initDocumentCursor document =
  case maxSentenceIndex document of
    Nothing -> DocumentUnsegmentedTextCursor
    Just i -> DocumentSentenceCursor i

diffSentenceIndex :: Document -> Document -> Maybe Word
diffSentenceIndex doc1 doc2 =
  do
    i <- maxSentenceIndex doc2
    go (documentSentences doc1) (documentSentences doc2) i
  where
    go :: Seq Sentence -> Seq Sentence -> Word -> Maybe Word
    go (xs :|> x) (ys :|> y) i
      | x /= y    = Just i
      | otherwise = if i > 0 then go xs ys (i - 1) else Nothing
    go _ _ _ = Nothing


validDocumentCursor :: Document -> DocumentCursor -> Bool
validDocumentCursor document (DocumentSentenceCursor i) =
  case maxSentenceIndex document of
    Nothing -> False
    Just j  -> i >= 0 && i <= j
validDocumentCursor _ DocumentUnsegmentedTextCursor = True

moveDocumentCursorLeft :: Document -> DocumentCursor -> Maybe DocumentCursor
moveDocumentCursorLeft _ (DocumentSentenceCursor i) | i > 0 = Just $ DocumentSentenceCursor (i - 1)
moveDocumentCursorLeft document DocumentUnsegmentedTextCursor | Just i <- maxSentenceIndex document = Just $ DocumentSentenceCursor i
moveDocumentCursorLeft _ _ = Nothing

moveDocumentCursorRight :: Document -> DocumentCursor -> Maybe DocumentCursor
moveDocumentCursorRight document (DocumentSentenceCursor i) | Just j <- maxSentenceIndex document =
  case compare i j of
    LT -> Just $ DocumentSentenceCursor (i + 1)
    EQ -> Just DocumentUnsegmentedTextCursor
    GT -> error "impossible: invalid DocumentSentenceCursor"
moveDocumentCursorRight _ _ = Nothing

