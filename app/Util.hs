module Util where

import Data.Sequence (Seq)
import GHC.Exts (IsList(fromListN))
import Data.Proxy (Proxy(Proxy))
import Data.Kind (Type, Constraint)
import qualified Data.Map as Map
import qualified Data.List as List
import Data.Tuple (swap)

mapping :: (Ord k, Ord v) => [(k, v)] -> (k -> v, v -> Maybe k)
mapping pairs = (\k -> m Map.! k, \v -> Map.lookup v m')
  where
    m  = Map.fromList pairs
    m' = Map.fromList (List.map swap pairs)

type ListOf :: (k -> Type) -> [k] -> Type
data ListOf f xs where
  Nil :: ListOf f '[]
  (:&) :: f x -> ListOf f xs -> ListOf f (x : xs)

infixr 5 :&

deriving instance (forall a. Eq (f a)) => Eq (ListOf f xs)
deriving instance (forall a. Show (f a)) => Show (ListOf f xs)

type Known :: Type -> Constraint
class Known a where
  known :: a

instance Known (Proxy a) where
  known = Proxy

instance Known (ListOf f '[]) where
  known = Nil

instance (Known (f x), Known (ListOf f xs)) => Known (ListOf f (x : xs)) where
  known = known :& known

data KV k v = k ::: v

type Index :: k -> v -> [KV k v] -> Type
data Index k v xs where
  IZ :: Index k v ((k ::: v) : xs)
  IS :: Index k v xs -> Index k v (y : xs)

deriving instance Show (Index k v xs)

indexOfEmpty :: Index k v '[] -> r
indexOfEmpty i = case i of {}

class KnownIndex k v xs | k xs -> v where
  knownIndex :: Index k v xs

instance (v ~ v') => KnownIndex k v ((k ::: v') : xs) where
  knownIndex = IZ

instance {-# OVERLAPPABLE #-} KnownIndex k v xs => KnownIndex k v (y : xs) where
  knownIndex = IS knownIndex

data SeqBuilder a = SeqBuilder !Int !([a] -> [a])

instance Semigroup (SeqBuilder a) where
  SeqBuilder n1 f1 <> SeqBuilder n2 f2 = SeqBuilder (n1 + n2) (f1 . f2)

instance Monoid (SeqBuilder a) where
  mempty = SeqBuilder 0 id

singletonSeqBuilder :: a -> SeqBuilder a
singletonSeqBuilder x = SeqBuilder 1 (x:)

runSeqBuilder :: SeqBuilder a -> Seq a
runSeqBuilder (SeqBuilder n f) = fromListN n (f [])

eitherToMaybe :: Either e a -> Maybe a
eitherToMaybe (Left _) = Nothing
eitherToMaybe (Right x) = Just x
